/*=======================================================================================================================
 * Nous allons ici nous connecter à un point d'accès eduspot pendant un court instant et juste après nous déconnecter,
 * puis nous allons nous connecter à un point d'accès RT et nous déconnecter,
 * à partir de là nous aurons deux adresses MAC de point d'accès à comparer afin de savoir
 * entre quelle salle et quelle autre salle la clé se trouve.
 * crédit : Dany SINAPAYEL
 *======================================================================================================================*/


//Librairie pour la localisation de la clé
#include <LWiFi.h>
#include <LWiFiClient.h>


#define WIFI_AP_LINK "LinkitOneWifi"            // WiFi AP SSID LINKITONE
#define WIFI_AP_EDU "kampus"            // WiFi AP SSID EDUSPOT (à modifier pour les nouveaux AP KAMPUS)
#define WIFI_AP "RT-WIFI-Guest"            // WiFi AP SSID RT WIFI GUEST
#define WIFI_PASSWORD "wifirtguest"  // WiFi AP RT mdp
#define WIFI_AUTH LWIFI_WPA           // type d'authentification              
char bssid1[18];             // @MAC de la borne WIFI
char bssid2[18];             // @MAC de la borne2 WIFI

char *rtwifi[6][2] = {{"80:2A:A8:5A:5C:68","Administration"},
                       {"80:2A:A8:4A:AD:26","Proj-doc"},
                       {"80:2A:A8:4A:AD:2E","Reseaux cablages"},
                       {"80:2A:A8:4A:AF:2E","Signaux Systemes"},
                       {"80:2A:A8:47:CE:A5","TD1"},
                       {"80:2A:A8:4A:AC:55","TD3"}};

char *eduwifi[7][2] = {{"00:1D:A2:D8:38:B1","Administration"},
                       {"00:07:0E:56:93:80","Proj-doc"},
                       /*{"00:1D:45:26:DD:7A","Reseaux cablages"},
                       {"00:14:A8:39:2C:B6","Signaux Systemes"},*/
                       /*{"00:13:1A:3A:B5:9C","TD1"}, // TD1: 00:1D:A2:D8:3D:E1*/
                       {"00:1D:A2:69:8E:9A","TD2"}, // a voir j'ai relever TD1 et TD2 identique
                       {"00:1D:A2:D8:3D:E1","Genie inf"}, // devrait etre en TD1
                       /*{"00:13:1A:96:7C:D0","Genie inf"},*/
                       {"00:13:1A:96:7D:71","Info prog"},
                       {"00:14:6A:F0:41:31","LPRO"},
                       {"00:14:6A:F0:42:21","LPRO"}};


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  LWiFi.begin();
}

void loop() {
 // partie pour la localisation
    //*** Connexion à eduspot ***\\
  LWiFi.begin();        // Initialise le module WiFi
  
  Serial.print("Connexion au WiFi kampus : \n");
  while (0 == LWiFi.connect(WIFI_AP_EDU)) //la clé se connecte au point d'accès eduspot le plus proche
  {
    delay(1000);
  }

  //Nous déterminons l'adresse MAC du point d'accès sur lequel la clé vient de se connecter
  StatutWifi();

  // On se déconnecte et on attend 5 secondes
  LWiFi.disconnect();
  delay(5000);
  Serial.println();

  //*** Connexion au RT-WIFI-Guest ****\\
  
  Serial.print("Connexion au WiFi ");
  Serial.println(WIFI_AP);
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }

  // Statut de connexion
  StatutWifi();

  // On se déconnecte et on attend 5 secondes
  LWiFi.disconnect();
  delay(5000);
  Serial.println();

  Localize(rtwifi,eduwifi);
  
}
// Affichage du statut WiFi
void StatutWifi() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI): ");
  Serial.print(rssi);
  Serial.println(" dBm");

  // print BSSID
  if (strcmp(LWiFi.SSID(),"RT-WIFI-Guest")==0){
    uint8_t BSSID[VM_WLAN_WNDRV_MAC_ADDRESS_LEN] = {0};
    LWiFi.BSSID(BSSID);
    Serial.print("BSSID2 is: ");  
    sprintf(bssid2, "%02X:%02X:%02X:%02X:%02X:%02X", BSSID[0], BSSID[1], BSSID[2], BSSID[3], BSSID[4], BSSID[5]);   
    Serial.println(bssid2);
  }
  else if (strcmp(LWiFi.SSID(),"kampus")==0){
    uint8_t BSSID[VM_WLAN_WNDRV_MAC_ADDRESS_LEN] = {0};
    LWiFi.BSSID(BSSID);
    Serial.print("BSSID1 is: ");  
    sprintf(bssid1, "%02X:%02X:%02X:%02X:%02X:%02X", BSSID[0], BSSID[1], BSSID[2], BSSID[3], BSSID[4], BSSID[5]);
    // Affichage du BSSID au bon format
    
    Serial.println(bssid1); 
  }

}

void Localize(char *rtwifi[6][2],char *eduwifi[7][2]){ 
  /*==============================================================
   * Ici nous rajouterons une condition disant:
   * - si un sms est reçu avec le numéro du secretariat 
   *    - On envoie notre localisation dans un sms à ce même numéro
   ===============================================================*/
  int i;
  for (i=0;i<6;i++){
    if (strcmp(bssid2,rtwifi[i][0])==0){
      Serial.println("---------------------------------");
      Serial.print("Je suis entre "); 
      Serial.print(rtwifi[i][1]);
      Serial.print(" ");
    }
  }

  for (i=0;i<7;i++){
    if (strcmp(bssid1,eduwifi[i][0])==0){
      Serial.print("et "); 
      Serial.print(eduwifi[i][1]);
      Serial.println("");
      Serial.println("--------------------------------");
    }
  }
}

