#include <rgb_lcd.h>        //on inclut la libraire pour la gestion de l'écran LCD
#include <Wire.h>           

rgb_lcd lcd;                //on crée un objet rgb_lcd pour pouvoir contrôler le LCD
const int colorR = 235;     //pour définir le code couleur
const int colorG = 245;
const int colorB = 255;

//variable pour contenir les entiers qui composent le numéro étudiant
int chiffre=0;
const int potPin = A0;
const int btnPin = A1;

int rangNumEtudiant = 0; //pour indéxer les chiffres du numéro étudiant
String numEtudiant = ""; //va contenir la totalité du numéro étudiant

int buttonPushCounter = 0;   // counter for the number of button presses
int buttonState = 0;         // current state of the button
int lastButtonState = 0;     // previous state of the button

void setup() {
  Serial.begin(9600);           //on démarre la communication série
  
  //on définit les pins sur lesquels sont reliés le potentiomètre et le bouton
  pinMode(potPin,INPUT);
  pinMode(btnPin,INPUT_PULLUP);
  
  //On définit le nombre de colonnes et de lignes que l'on aura pour le LCD
  lcd.begin(16, 2);
  /*
  lcd.setRGB(colorR, colorG, colorB); //définition des couleurs
  
  lcd.print("Num etudiant: ");
  lcd.setCursor(0, 1);
  */
  
}

void loop() {
  //on lit la valeur du bouton
  buttonState = digitalRead(btnPin);
  chiffre = map(analogRead(potPin), 0,1023, 0,9);

  //=======saisi du numéro étudiant==============
  if(rangNumEtudiant > 7){ //si le numéro est complet, on valide l'empreint 
    emprunt();
    
    
  }else{
    buttonState = digitalRead(btnPin); //on lit la valeur du bouton
    lcd.setCursor(0, 0);
    lcd.setRGB(colorR, colorG, colorB); //définition des couleurs
    lcd.print("Num etudiant: ");
    
    //on se place sur la deuxième ligne de l'écran
    lcd.setCursor(rangNumEtudiant, 1);
    lcd.print(chiffre);//on affiche le chiffre que pointe actuellement le potentiomètre
    if(buttonState != lastButtonState){ //si il y a un changement d'état du bouton
      if(buttonState == LOW){
         rangNumEtudiant++;
         numEtudiant += chiffre;
         //Serial.println(numEtudiant);//pour afficher le numéro étudiant qui se construit
      }
      lastButtonState = buttonState;

      
    }
  }
}
boolean emprunt(){
  lcd.clear();
  lcd.print("empreint valide");
  lcd.setRGB(0, colorG, 155);
  //on lit la valeur du bouton
   buttonState = digitalRead(btnPin);
   if(buttonState != lastButtonState){ //si il y a un changement d'état du bouton
      if(buttonState == LOW){
         buttonPushCounter++;
      }
      lastButtonState = buttonState; 
    }
  if(buttonPushCounter == 2){
       buttonPushCounter = 0;
       rangNumEtudiant = 0;
       lcd.clear();
       return false;
  }
  return true;  
}


